---
title: "About"
date: 2020-09-02T11:58:04+01:00
draft: false
sidebar: "right" # Enable sidebar (on the right side) per page
menu: main
---

In this section I will talk about me and this website, what it is, what it isn't and some more information about me.

## What this website is:

* A place for me to store information
* A place for me to practice my HTML skills (which are admittedly limited)
* And finally, a place to share information

## What this website is not:

* A place providing previously unknown information
* Necessarily correct, I won't always have all of the facts
* A website that can be relied on to exist into the future

## What this Website is About:

This website is perhaps not as well defined in its goals as many other websites.
The short answer is that it will be about whatever I want it to be about.
This will mean that it will largely cover my interest but I will be keeping it roughly sectioned out mostly to make it easier to find information that you care out (because you won't care about all of it).

I will cover (roughly):

* Computer programs (mostly open-source)
* Linux
* Programing (but don't expect anything particularly advanced)
* Other computer-related projects

## Contact Me

With this website, I want to hear from YOU.
I will have a email set up for with website which can be found at the address tristan_hodgson@pm.me (yes I am using proton mail)
I want to hear about feature requests and requests for things I should cover, bugs and what you like about the website.
But overall I just want to hear from you.

## Technical Details

This section is probably of no interest to most people so I will keep it short it is about the technical details about this website.
I am using Netlify to host my website and Namecheap for my domain.
To create the HTML, CSS and JS I am using the Hugo static site generator with the Mainroad theme.
