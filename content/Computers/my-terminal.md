---
title: "My Terminal"
date: 29/07/2020
description: "I wanted to create this articles as a record of how I currently have my terminal emulator set up."
lead: "I wanted to create this articles as a record of how I currently have my terminal emulator set up."
draft: false
categories:
  - "Linux"
  - "Applications"
tags:
  - "Configs"
thumbnail: "img/my-terminal.png" # Thumbnail image
sidebar: "right" # Enable sidebar (on the right side) per page
---

## Alacritty:

I use [alacritty](https://github.com/alacritty/alacritty) as my terminal emulator because it is fast, even if I can't claim that I notice that it is supposedly the fastest.
This is important to me because if I want to use my terminal it is because I won't to do something quickly therfore waiting around for terminal to load is not particually appealing.
Inside of alacritty I use the nord colour theme because it is a nice-looking, cool-coloured colour scheme. 

## ZSH:

If I honest I am not sure why I use [ZSH](https://www.zsh.org/). I think I tried it and then never switched back.
Now I use it because I don't really care.
I have my ZSH config stored in ~/.config/zsh which you may know is different to the default mess of files in my home folder.
To do this yourself you add the line `export ZDOTDIR=$HOME/.config/zsh` to your ~/.zshenv file.
Starship

I use the [Starship](https://starship.rs/) cross-shell prompt because it is more easily customisable than many other prompts.
Also, I like that it is cross-shell because it means that if I ever get a reason to change shells then I can keep my prompt with no changes.
I have my prompt set up to shoe me my working path and then a ❯ symbol on the left all using my terminal's cyan colour.
I like this simple prompt because it doesn't distract me and due to how little I need to see other information.
Then again I may end up adding a git part to my prompt because I hope to get better at uploading things to my github.

## Ufetch:

Many people use a program call [Neofetch](https://github.com/dylanaraps/neofetch) in order to display a picture (made of characters) of their distribution's logo and show basic system information.
But while this always appealed to me but it slowed the startup of my terminal to a crawl (I may be exaggerating just a bit).
My solution a program called [Ufetch](https://gitlab.com/jschx/ufetch).
The only disadvantage of this program over a program like neofetch is that it cannot auto detect the distribution you are using.

While there is a program called [Pfetch](https://github.com/dylanaraps/pfetch) that is the same underlying program as Ufetch but with auto detection, I do not use it because I don't change distribution much so it barely matters. 
