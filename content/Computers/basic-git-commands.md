---
title: "Basic Git Commands"
date: 23/07/2020
draft: false
description: "A quick cheatsheet for practically using git in a project"
lead: "A quick cheatsheet for practically using git in a project"
categories:
  - "Programming"
tags:
  - "Git"
  - "Tutorials"
thumbnail: "img/basic-git-commands.jpg" # Thumbnail image
sidebar: "right" # Enable sidebar (on the right side) per page
---

## What is Git:

Git is a command line utility that is used for version control.
But what does this mean?
It means that it is the solution (there are others but none quite as good) to maintaining source code.
It allows many versions of a project to be kept without making full copies of the source code every time you make changes.

Git can also link to git servers such as Github, Gitlab, Bitbucket or your own git server.
This can be useful for collaberation because many people can contribute code to the project without you using a regular cloud storage providor that does not include these festures.

## How Do You Use It:

Git has a reputation of being a difficult program to use partly because it is terminal (or command prompt) only and that it has a somewhat confusing set of terminology.

While I will not adress that last problem in this article (if you wish we can watch this youtube video that explains it well).
But I will provide a cheatsheet to the magic words to make the whole system work.

## The Cheatsheet:

| Command                                    | Description                                   |
|--------------------------------------------|-----------------------------------------------|
| `git init                                  ` | Turn the current folder into a git repo       |
| `git add file                              ` | Add a file to the staging area                |
| `git add .                                 ` | Add all files in a folder to a repo           |
| `git commit                                ` | Create a snapshot of the repo (add a message) |
| `git merge branch1 branch2                 ` | Merge two branches together                   |
| `git push remote branch:remote-branch      ` | Push the committed changes to a git server    |
| `git branch new-branch-name                ` | Create a new branch for a repository          |
| `git checkout branch-name                  ` | Switch to looking at a different branch       |
| `git log --graph --all --decorate --oneline` | Get a visualisation of commits and branches   |
| `git clone url                             ` | Copy a git remote directory to your computer  |

I hope that this cheatsheet will help you because it certianly will help me.
I use github as part of the hosting for this website and consantly forget even the most basic of git commands.
So as much as I invite you to use this cheatsheet it is just as much for me as for you.
