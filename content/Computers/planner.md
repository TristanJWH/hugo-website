---
title: "Planner"
date: 28/10/2020
description: "I wanted to show an application that I use to improve my orgainisation: planner."
lead: "I wanted to show an application that I use to improve my orgainisation: planner."
draft: false
categories:
  - "Linux"
  - "Applications"
thumbnail: "img/planner.png" # Thumbnail image
sidebar: "right" # Enable sidebar (on the right side) per page
---

# What is Planner

[Planner](https://planner-todo.web.app/) is a very simple application in principle, it is a task based organisational system.
However, in practice it can be a little confusing to understand and use it in the most effective way possible.
The way that I do this (and I will emphasise that my way is in no way the way for everyone) is by organising all of my tasks into groups, so I can decide which tasks I need and want to do at any specific moment.

# Levels of Organisation

If that system sounds good to you, I will tell you more precisely how I organise my tasks so that I can get them done.

## Projects

Planner allows you to create projects and project groups in the sidebar of the application.
I use this projects to organise tasks into, well, projects.
For example, I have a website project where I can put any ideas for articles and tick them off when they are completed.

## Tags

Planner allows you to add tags to specific tasks to further group tasks.
I use this to divide a task into what type of task it is.
For example, I might divide tasks that require me to write an article with the tag called article.

# Other Important Features

Planner does have several other important features, most of which I do not use but I will highlight them here anyway.  
* Backup to ToDoist
* Quick add for task
* Checklists in tasks
* Due dates
* Reminders
* Priority of tasks

# How to install

The main way to install Planner is through [Flathub](https://flathub.org/apps/details/com.github.alainm23.planner) though if you are on Elementary OS it is available through the app store.  
To install through flathub use the command:

```Bash
flatpak install flathub com.github.alainm23.planner
```

This will install Planner on any distribution that has [Flatpak set up](https://flatpak.org/setup/) (that is nearly any distro).
